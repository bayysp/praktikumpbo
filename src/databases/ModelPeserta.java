package databases;

import javax.swing.*;
import java.sql.*;

public class ModelPeserta {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/perpustakaan";
    static final String USER = "root";
    static final String PASS = "";

    Connection koneksi;
    Statement statement;

    public ModelPeserta() {
        try {
            Class.forName(JDBC_DRIVER);
            koneksi = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("KONEKSI BERHASIL");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
            System.out.println("KONEKSI GAGAL");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            System.out.println("KONEKSI GAGAL");
        }
    }

    public void inputData(String nama, String nim, String angkatan, String jurusan) {
        try {
            String query = "INSERT INTO `peserta`(`nama`, `nim`, `angkatan`, `jurusan`) VALUES ('" + nama + "','" + nim + "','" + angkatan + "','" + jurusan + "')";
            statement = koneksi.createStatement();
            statement.executeUpdate(query);
            System.out.println("Sukses");

            JOptionPane.showMessageDialog(null, "Tambahkan Produk Baru Sukses!");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public int getBanyakData() {
        try {
            int jmlData = 0;
            statement = koneksi.createStatement();

            String query = "SELECT * FROM peserta";

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                jmlData++;
            }
            return jmlData;
        } catch (Exception e) {
            System.out.println("Gagal Ambil JUmlah Data");
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public String[][] readMahasiswa() {
        int indexBaris = 0;
        String dataMahasiswa[][] = new String[getBanyakData()][4];
        try {
            statement = koneksi.createStatement();

            String query = "SELECT * FROM peserta";

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                dataMahasiswa[indexBaris][0] = resultSet.getString("nama");
                dataMahasiswa[indexBaris][1] = resultSet.getString("nim");
                dataMahasiswa[indexBaris][2] = resultSet.getString("angkatan");
                dataMahasiswa[indexBaris][3] = resultSet.getString("jurusan");
                indexBaris++;
            }
            return dataMahasiswa;
        } catch (Exception e) {
            System.out.println("Gagal Ambil Jumlah Data");
            System.out.println(e.getMessage());
        }
        return dataMahasiswa;
    }
}
