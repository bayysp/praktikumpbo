package databases;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormInputData extends JFrame{
    JLabel lKodeProduk = new JLabel("Nama    :");
    JTextField tfNama = new JTextField();
    JLabel lNamaproduk = new JLabel("NIM   :");
    JTextField tfNim = new JTextField();
    JLabel lHargaProduk = new JLabel("Angkatan  :");
    JTextField tfAngkatan = new JTextField();
    JLabel lStokProduk = new JLabel("Jurusan     :");
    JTextField tfJurusan = new JTextField();

    JButton btnTambahPanel = new JButton("Tambah");
    JButton btnBatalPanel = new JButton("Batal");

    Object namaKolom[] = {"Nama","NIM","Angkatan","Jurusan"};
    DefaultTableModel defaultTableModel = new DefaultTableModel(namaKolom,0);
    JTable table = new JTable(defaultTableModel);
    JScrollPane scrollPane = new JScrollPane(table);

    ModelPeserta modelPeserta = new ModelPeserta();

    public FormInputData(){

        setVisible(true);
        setLayout(null);
        setSize(700, 700);
        setBackground(Color.white);

        add(lKodeProduk);
        lKodeProduk.setBounds(5, 5, 90, 20);
        add(tfNama);
        tfNama.setBounds(110, 5, 120, 20);
        add(lNamaproduk);
        lNamaproduk.setBounds(5, 30, 90, 20);
        add(tfNim);
        tfNim.setBounds(110, 30, 120, 20);
        add(lHargaProduk);
        lHargaProduk.setBounds(5, 55, 90, 20);
        add(tfAngkatan);
        tfAngkatan.setBounds(110, 55, 120, 20);
        add(lStokProduk);
        lStokProduk.setBounds(5, 80, 90, 20);
        add(tfJurusan);
        tfJurusan.setBounds(110, 80, 120, 20);

        add(btnTambahPanel);
        btnTambahPanel.setBounds(20, 105, 90, 20);
        btnTambahPanel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String nama = tfNama.getText();
                    String nim = tfNim.getText();
                    String angkatan = tfAngkatan.getText();
                    String jurusan = tfJurusan.getText();

                    modelPeserta.inputData(nama,nim,angkatan,jurusan);
                    updateTable();
                }catch (Exception ex){
                    System.out.println("INPUTAN ERROR");
                    System.out.println(ex.getMessage());
                }
            }
        });

        add(btnBatalPanel);
        btnBatalPanel.setBounds(130, 105, 90, 20);

        //setting table
        add(scrollPane);
        scrollPane.setBounds(20,160,620,300);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        updateTable();

    }

    public void updateTable(){

        if(modelPeserta.getBanyakData() != 0){
            String dataMhs[][] = modelPeserta.readMahasiswa();
            table.setModel((new JTable(dataMhs, namaKolom)).getModel());
        }else{
            JOptionPane.showMessageDialog(null, "Data Tidak Ada");
        }

    }
}
